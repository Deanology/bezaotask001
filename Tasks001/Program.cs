﻿
using System;
using Tasks001.Services;

namespace Tasks001
{
    class Program
    {
        static void Main(string[] args)
        {
            string fullname = GetUser.getUser();
            int age = GetUser.getAge();


            Console.WriteLine($"My name is {fullname}, and i am {age} years old. ");
            Console.WriteLine("My name is {0}, and i am {1} years old. ", fullname , age);
            Console.ReadLine();
        }
     
    }
}
