﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tasks001.Services
{
    class GetUser
    {
        public static string getUser()
        {
            //get and read user details
            Console.WriteLine("Enter Your Full name");
            Console.WriteLine("================================");
            string fullname = Console.ReadLine();
            return fullname;
        }
        public static int getAge()
        {
            //declare and assign age variable
            int age = 0;

            //get and read user date of birth
            Console.WriteLine("Enter your Date of Birth in the format DD/MM/YYYY OR DD-MM-YYYY OR DD MM YYYY");
            Console.WriteLine("================================");
            string dateofbirth = Console.ReadLine();
          

            try
            {
                DateTime bday = Convert.ToDateTime(dateofbirth);
                DateTime today = DateTime.Today;

                 age = today.Year - bday.Year;
                if (bday > today.AddYears(-age))
                    age--;
            }
            catch(Exception e)
            {
                Console.WriteLine(e.Message);
            }
            return age;
        }
    }
}
